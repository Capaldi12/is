alphabet = 'АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ.,- '
indices = 'АБВГДЕ'

square = [list(alphabet[i*6:i*6+6]) for i in range(6)]

lookup_encode = {square[i][j]: indices[i] + indices[j]
                 for i in range(6) for j in range(6)}

lookup_decode = {indices[i] + indices[j]: square[i][j]
                 for i in range(6) for j in range(6)}


def slice_by(line, n):
    return (line[i:i+n] for i in range(0, len(line), n))


def encode(text):
    return ''.join(lookup_encode.get(c, '') for c in text.upper())


def decode(text):
    return ''.join(lookup_decode.get(c, '') for c in slice_by(text.upper(), 2))


msg = 'Это секретное сообщение'

enc = encode(msg)

dec = decode(enc)

print(f'{msg=}\n{enc=}\n{dec=}')
