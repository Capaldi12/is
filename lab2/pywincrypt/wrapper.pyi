from typing import Tuple

def sign(data: bytes) -> Tuple[bytes, bytes]:
    """Signs data using default container, returns signature and public key."""

def verify(data: bytes, signature: bytes, public_key: bytes) -> bool:
    """Verifies signature for given data using given public key."""

def gimme_error() -> None:
    """Raises wrapper.error error."""

class error(Exception):
    pass
