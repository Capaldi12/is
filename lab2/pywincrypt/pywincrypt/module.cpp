#define PY_SSIZE_T_CLEAN
#include <Python.h>

// --------------------
// Imports

#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string.h>

#include <windows.h>
#include <Wincrypt.h>

// --------------------
// Custom exception

static PyObject* WrapperError;

// --------------------
// Main functions

static PyObject* signData(PyObject* self, PyObject* args) {

    PyObject* ret = NULL;

    PyObject* py_sig = NULL;
    PyObject* py_key = NULL;

    const char* data;
    Py_ssize_t dataLen;

    char* keyExport = nullptr;
    DWORD keyLen = 0;

    char* signature = nullptr;
    DWORD sigLen = 0;

    HCRYPTPROV prov = NULL;
    HCRYPTKEY key = NULL;
    HCRYPTHASH hash = NULL;

    // Parse python byte string to char array
    if (!PyArg_ParseTuple(args, "y#", &data, &dataLen)) {

        ret = NULL; goto end;
    }
    
    // Get conext
    if (!CryptAcquireContext(&prov, NULL, NULL, PROV_RSA_FULL, 0)) {

        PyErr_SetString(WrapperError, "Unable to acquire context");
        ret = NULL; goto end;
    }

    // Get or generate user key
    if (!CryptGetUserKey(prov, AT_SIGNATURE, &key)) {

        if (GetLastError() == NTE_NO_KEY) {

            if (!CryptGenKey(prov, AT_SIGNATURE, CRYPT_ARCHIVABLE | CRYPT_EXPORTABLE, &key)) {

                PyErr_SetString(WrapperError, "Unable to generate user key");
                ret = NULL; goto end;
            }

        }
        else {

            PyErr_SetString(WrapperError, "Unable to get user key");
            ret = NULL; goto end;
        }
    }

    // Create hash object
    if (!CryptCreateHash(prov, CALG_MD5, 0, 0, &hash)) {

        PyErr_SetString(WrapperError, "Unable to create hash object");
        ret = NULL; goto end;
    }

    // Hash data
    if (!CryptHashData(hash, (BYTE*)data, (DWORD)dataLen, 0)) {

        PyErr_SetString(WrapperError, "Unable to hash data");
        ret = NULL; goto end;
    }

    // Sign hashed data
    if (!CryptSignHash(hash, AT_SIGNATURE, NULL, 0, NULL, &sigLen)) {

        PyErr_SetString(WrapperError, "Unable get signature length");
        ret = NULL; goto end;
    }

    if (!(signature = (char*)malloc(sigLen))) {

        PyErr_SetString(WrapperError, "Unable to allocate memory for signature");
        ret = NULL; goto end;
    }

    if (!CryptSignHash(hash, AT_SIGNATURE, NULL, 0, (BYTE*)signature, &sigLen)) {

        PyErr_SetString(WrapperError, "Unable to create signature");
        ret = NULL; goto end;
    }

    // Export public key
    if (!CryptExportKey(key, NULL, PUBLICKEYBLOB, 0, NULL, &keyLen)) {

        PyErr_SetString(WrapperError, "Unable get public key length");
        ret = NULL; goto end;
    }

    if (!(keyExport = (char*)malloc(keyLen))) {
        
        PyErr_SetString(WrapperError, "Unable to allocate memory for key export");
        ret = NULL; goto end;
    }

    if (!CryptExportKey(key, NULL, PUBLICKEYBLOB, 0, (BYTE*)keyExport, &keyLen)) {
        
        PyErr_SetString(WrapperError, "Unable to export public key");
        ret = NULL; goto end;
    }

    // Construct return value
    py_sig = PyBytes_FromStringAndSize(signature, (Py_ssize_t)sigLen);
    if (!py_sig) {

        //PyErr_SetString(WrapperError, "Unable to construct signature python object");
        ret = NULL; goto end;
    }
    else
        Py_XINCREF(py_sig);

    py_key = PyBytes_FromStringAndSize(keyExport, (Py_ssize_t)keyLen);
    if (!py_key) {

        //PyErr_SetString(WrapperError, "Unable to construct key export python object");
        ret = NULL; goto end;
    }
    else
        Py_XINCREF(py_key);

    ret = PyTuple_Pack(2, py_sig, py_key);

end:

    if (signature) free(signature);
    if (keyExport) free(keyExport);

    if (hash) CryptDestroyHash(hash);
    if (key) CryptDestroyKey(key);
    if (prov) CryptReleaseContext(prov, 0);

    Py_XINCREF(ret);
    return ret;
}

static PyObject* verifySignature(PyObject* self, PyObject* args) {

    PyObject* ret = NULL;
    bool res;

    const char* data = nullptr;
    Py_ssize_t dataLen = 0;

    const char* keyImport = nullptr;
    Py_ssize_t keyLen = 0;

    const char* signature = nullptr;
    Py_ssize_t sigLen = 0;

    HCRYPTPROV prov = NULL;
    HCRYPTKEY key = NULL;
    HCRYPTHASH hash = NULL;

    // Parse arguments
    if (!PyArg_ParseTuple(args, "y#y#y#", 
                          &data, &dataLen, 
                          &signature, &sigLen, 
                          &keyImport, &keyLen)
        ) {

        ret = NULL; goto end;
    }

    // Get conext
    if (!CryptAcquireContext(&prov, NULL, NULL, PROV_RSA_FULL, 0)) {

        PyErr_SetString(WrapperError, "Unable to acquire context");
        ret = NULL; goto end;
    }

    // Create hash object
    if (!CryptCreateHash(prov, CALG_MD5, 0, 0, &hash)) {

        PyErr_SetString(WrapperError, "Unable to create hash object");
        ret = NULL; goto end;
    }

    // Hash data
    if (!CryptHashData(hash, (BYTE*)data, (DWORD)dataLen, 0)) {

        PyErr_SetString(WrapperError, "Unable to hash data");
        ret = NULL; goto end;
    }

    // Import public key
    if (!CryptImportKey(prov, (BYTE*)keyImport, keyLen, 0, 0, &key)) {

        PyErr_SetString(WrapperError, "Unable to import key");
        ret = NULL; goto end;
    }

    // Verify signature
    if (!CryptVerifySignature(hash, (BYTE*)signature, sigLen, key, NULL, 0)) {

        if (GetLastError() == NTE_BAD_SIGNATURE)
            res = false;
        else {

            PyErr_SetString(WrapperError, "Unable to verify signature");
            ret = NULL; goto end;
        }
    }
    else
        res = true;

    // Select appropriate python object
    if (res)
        ret = Py_True;
    else
        ret = Py_False;

end:

    if (hash) CryptDestroyHash(hash);
    if (key) CryptDestroyKey(key);
    if (prov) CryptReleaseContext(prov, 0);

    Py_XINCREF(ret);
    return ret;
}

static PyObject* getError(PyObject* self) {

    PyErr_SetString(WrapperError, "Here you go!");
    return NULL;
}

// --------------------
// Python module declaration/initialization

// Module methods
static PyMethodDef wrapper_methods[] = {
    {"sign", (PyCFunction)signData, METH_VARARGS,
        PyDoc_STR("Signs data using default container, "
                  "returns signature and public key.")},

    {"verify", (PyCFunction)verifySignature, METH_VARARGS, 
        PyDoc_STR("Verifies signature for given "
                  "data using given public key.")},

    {"gimme_error", (PyCFunction)getError, METH_NOARGS,
        PyDoc_STR("Raises wrapper.error error.")},

    {NULL, NULL, 0, NULL}
};

// Module parameters?
static PyModuleDef wrapper_module = {
    PyModuleDef_HEAD_INIT, "wrapper", NULL, -1, wrapper_methods
};

// Module init
PyMODINIT_FUNC PyInit_wrapper(void) {

    PyObject* m = PyModule_Create(&wrapper_module);

    if (m == NULL)
        return NULL;

    // Create exception raised by module
    WrapperError = PyErr_NewException("wrapper.error", NULL, NULL);
    Py_XINCREF(WrapperError);

    if (PyModule_AddObject(m, "error", WrapperError) < 0) {
        Py_XDECREF(WrapperError);
        Py_CLEAR(WrapperError);
        Py_DECREF(m);
        return NULL;
    }

    return m;
}
