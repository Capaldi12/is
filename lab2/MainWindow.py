from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, \
    QMessageBox, QFileDialog, QTextEdit

from PyQt5.QtCore import pyqtSignal, pyqtBoundSignal
from PyQt5.QtGui import QDropEvent, QDragEnterEvent, QDragLeaveEvent

from MainWindowUi import Ui_MainWindow


import os

import clipboard

import pywincrypt.wrapper


class BadContentError(ValueError):
    pass


class MyTextEdit(QTextEdit):
    on_drop: pyqtBoundSignal = pyqtSignal(str)

    def __init__(self, parent: QWidget, text: str = None):
        if text:
            super().__init__(text, parent)
        else:
            super().__init__(parent)

        self.setReadOnly(True)

    def dragEnterEvent(self, e: QDragEnterEvent) -> None:
        self.setReadOnly(False)
        e.acceptProposedAction()

    def dragLeaveEvent(self, e: QDragLeaveEvent) -> None:
        self.unsetCursor()
        self.clearFocus()
        self.setReadOnly(True)

    def dropEvent(self, e: QDropEvent) -> None:
        super().dropEvent(e)
        self.undo()

        if e.mimeData().hasText():
            self.on_drop.emit(e.mimeData().text())
            e.accept()

        else:
            e.ignore()

        self.setReadOnly(True)


class MainWindow(QMainWindow, Ui_MainWindow):
    _sig_len = 128
    _key_len = 148

    filename: str
    _file_content: bytes = None

    signature_filename: str
    _signature: bytes = None

    key_filename: str
    _key: bytes = None

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.setupUi(self)

        # File preview widget
        self.file_view = MyTextEdit(self)
        self.file_view.setPlaceholderText(
            'Click Load button to select file or drag and drop it here'
        )
        self.file_view.setMinimumSize(250, 150)
        self.file_view.on_drop.connect(self.on_file_drop)

        self.file_view_layout.addWidget(self.file_view)

        # Signature view widget
        self.signature_view = MyTextEdit(self)
        self.signature_view.setPlaceholderText(
            'Click Load button to select file with signature, drop it here or '
            'drop/paste signature text.'
        )

        self.signature_view.setMinimumSize(250, 150)
        self.signature_view.on_drop.connect(self.on_signature_drop)

        self.signature_view_layout.addWidget(self.signature_view)

        # Public key view widget
        self.key_view = MyTextEdit(self)
        self.key_view.setPlaceholderText(
            'Click Load button to select file '
            'with public key, drop it here or '
            'drop/paste public key text.'
        )

        self.key_view.setMinimumSize(250, 150)
        self.key_view.on_drop.connect(self.on_key_drop)

        self.key_view_layout.addWidget(self.key_view)

        self.load_file_button.clicked.connect(lambda: self.on_load_file(0))
        self.load_signature_button.clicked.connect(lambda: self.on_load_file(1))
        self.load_key_button.clicked.connect(lambda: self.on_load_file(2))

        self.save_signature_button.clicked.connect(lambda: self.on_save_file(0))
        self.save_key_button.clicked.connect(lambda: self.on_save_file(1))

        self.copy_signature_button.clicked.connect(lambda: self.on_copy(0))
        self.copy_key_button.clicked.connect(lambda: self.on_copy(1))

        self.paste_signature_button.clicked.connect(lambda: self.on_paste(0))
        self.paste_key_button.clicked.connect(lambda: self.on_paste(1))

        self.sign_button.clicked.connect(self.on_sign)
        self.verify_button.clicked.connect(self.on_verify)

        self.initially_disable()

    def initially_disable(self):
        self.save_signature_button.setDisabled(True)
        self.save_key_button.setDisabled(True)

        self.copy_signature_button.setDisabled(True)
        self.copy_key_button.setDisabled(True)

        self.sign_button.setDisabled(True)
        self.verify_button.setDisabled(True)

    # Binary data properties
    @property
    def file_content(self) -> bytes:
        return self._file_content

    @file_content.setter
    def file_content(self, value: bytes):
        self._file_content = value

        self.sign_button.setEnabled(True)

        self.check_for_verify()

    @property
    def signature(self) -> bytes:
        return self._signature

    @signature.setter
    def signature(self, value: bytes):
        self._signature = value
        self.signature_view.setText(self.signature.hex().upper())

        self.save_signature_button.setEnabled(True)
        self.copy_signature_button.setEnabled(True)

        self.check_for_verify()

    @property
    def key(self) -> bytes:
        return self._key

    @key.setter
    def key(self, value: bytes):
        self._key = value
        self.key_view.setText(self.key.hex().upper())

        self.save_key_button.setEnabled(True)
        self.copy_key_button.setEnabled(True)

        self.check_for_verify()

    # Check if can verify signature
    def check_for_verify(self):
        self.verify_button.setEnabled(bool(
            self.file_content and self.signature and self.key
        ))

    # Load button press handler
    def on_load_file(self, btn: int):
        options = [
            ('Select file to sign/verify', None,
             self.set_file, '', ''),

            ('Select signature file',
             'Signature files (*.sig);;All Files (*)',
             self.set_signature, 'Bad signature',
             'File does not contain signature in valid format!'),

            ('Select public key file',
             'Public key files (*.key);;All Files (*)',
             self.set_key, 'Bad key',
             'File does not contain public key in valid format!'),
        ]

        caption, filter_, set_func, title, text = options[btn]

        filename = QFileDialog.getOpenFileName(self, caption, '', filter_)

        if filename[0]:
            try:
                set_func(filename=filename[0])

            except FileNotFoundError:
                QMessageBox.warning(
                    self, 'Bad file',
                    'Specified path does not exists or is not a file!'
                )
            except BadContentError:
                QMessageBox.warning(self, title, text)

    # Save button press handler
    def on_save_file(self, btn: int):
        options = [
            ('Select file to save signature',
             'Signature files (*.sig);;All Files (*)',
             'signature.sig', self.signature),

            ('Select file to save public key',
             'Public key files (*.key);;All Files (*)',
             'public.key', self.key),
        ]

        caption, filter_, directory, prop = options[btn]

        filename = QFileDialog.getSaveFileName(
            self, caption, directory, filter_)

        if filename[0]:
            with open(filename[0], 'wb') as file:
                file.write(prop)

    # Paste button press handler
    def on_paste(self, btn: int):
        clip = clipboard.paste()

        try:
            text = bytes.fromhex(clip)

            if btn == 0:
                self.set_signature(text=text)
            else:
                self.set_key(text=text)

        except BadContentError:
            QMessageBox.warning(
                self, 'Bad ' + ('signature' if btn == 0 else 'key'),
                ('Signature' if btn == 0 else 'Key') + ' format is not valid!'
            )

        except ValueError:
            QMessageBox.warning(
                self, 'Bad ' + ('signature' if btn == 0 else 'key'),
                ('Signature' if btn == 0 else 'Key') + ' must be in hex format!'
            )

    # Copy button press handler
    def on_copy(self, btn: int):
        if btn == 0:
            clipboard.copy(self.signature.hex())
        else:
            clipboard.copy(self.key.hex())

    # Drag and drop handlers
    def on_file_drop(self, text: str):

        if text[:8] == 'file:///':
            try:
                self.set_file(text[8:])

            except FileNotFoundError:
                QMessageBox.warning(
                    self, 'Bad file',
                    'Specified path does not exists or is not a file!'
                )
        else:
            QMessageBox.warning(
                self, 'Not a file',
                'This is not a file!'
            )

    def on_signature_drop(self, text: str):

        if text[:8] == 'file:///':
            try:
                self.set_signature(filename=text[8:])

            except FileNotFoundError:
                QMessageBox.warning(
                    self, 'Bad file',
                    'Specified path does not exists or is not a file!'
                )
            except BadContentError:
                QMessageBox.warning(
                    self, 'Bad signature',
                    'File does not contain signature in valid format!'
                )

        else:
            try:
                sig = bytes.fromhex(text)

                self.set_signature(text=sig)

            except BadContentError:
                QMessageBox.warning(
                    self, 'Bad signature',
                    'Signature format is not valid!'
                )

            except ValueError:
                QMessageBox.warning(
                    self, 'Bad signature',
                    'Signature must be in hex format!'
                )

    def on_key_drop(self, text: str):

        if text[:8] == 'file:///':
            try:
                self.set_key(filename=text[8:])

            except FileNotFoundError:
                QMessageBox.warning(
                    self, 'Bad file',
                    'Specified path does not exists or is not a file!'
                )
            except BadContentError:
                QMessageBox.warning(
                    self, 'Bad key',
                    'File does not contain public key in valid format!'
                )

        else:
            try:
                key = bytes.fromhex(text)

                self.set_key(text=key)

            except BadContentError:
                QMessageBox.warning(
                    self, 'Bad key',
                    'Public key format is not valid!'
                )

            except ValueError:
                QMessageBox.warning(
                    self, 'Bad key',
                    'Public key must be in hex format!'
                )

    # Check and set file/signature/key from any possible way
    def set_file(self, filename):

        if os.path.exists(filename) and os.path.isfile(filename):
            self.filename = filename

            with open(filename, 'rb') as file:
                self.file_content = file.read()

            self.file_view.setText(f'File: {filename}\n\nContent:\n'
                                   f'{self.file_content.hex().upper()}')

        else:
            raise FileNotFoundError('Path does not exists or is not a file')

    def set_signature(self, *, filename: str = None, text: bytes = None):

        if filename is not None:
            if os.path.exists(filename) and os.path.isfile(filename):
                with open(filename, 'rb') as file:
                    text = file.read()

                if self.is_valid_signature(text):
                    self.signature = text
                    self.signature_filename = filename

                else:
                    raise BadContentError(
                        'File does not contain valid signature')

            else:
                raise FileNotFoundError('Path does not exists or is not a file')

        elif text is not None:
            if self.is_valid_signature(text):
                self.signature = text

            else:
                raise BadContentError(
                    'Specified signature not in valid format')

        else:
            raise ValueError(
                'At least one of filename or text should be provided')

    def set_key(self, *, filename: str = None, text: bytes = None):

        if filename is not None:
            if os.path.exists(filename) and os.path.isfile(filename):
                with open(filename, 'rb') as file:
                    text = file.read()

                if self.is_valid_key(text):
                    self.key = text
                    self.key_filename = filename

                else:
                    raise BadContentError(
                        'File does not contain valid public key')

            else:
                raise FileNotFoundError(
                    'Path does not exists or is not a file')

        elif text is not None:
            if self.is_valid_key(text):
                self.key = text

            else:
                raise BadContentError(
                    'Specified public key not in valid format')

        else:
            raise ValueError(
                'At least one of filename or text should be provided')

    # Key/signature validation
    def is_valid_signature(self, signature: bytes) -> bool:
        return len(signature) == self._sig_len

    def is_valid_key(self, key: bytes) -> bool:
        return len(key) == self._key_len

    # Main functions - sign and verify file signature
    def on_sign(self):
        try:
            self.signature, self.key = \
                pywincrypt.wrapper.sign(self.file_content)

            QMessageBox.information(
                self, 'Success',
                'File was successfully signed'
            )

        except pywincrypt.wrapper.error as e:
            QMessageBox.critical(
                self, 'Error',
                e.args[0]
            )

    def on_verify(self):
        try:
            verified = pywincrypt.wrapper.verify(
                self.file_content, self.signature, self.key
            )

            if verified:
                QMessageBox.information(
                    self, 'Verified',
                    'Signature matches file. File is genuine.'
                )

            else:
                QMessageBox.critical(
                    self, 'Not verified',
                    'Signature does not match file. File was modified.'
                )

        except pywincrypt.wrapper.error as e:
            QMessageBox.critical(self, 'Error', e.args[0])


if __name__ == '__main__':
    a = QApplication([])
    w = MainWindow()

    w.show()
    a.exec()
